import { newE2EPage, E2EPage } from '@stencil/core/testing';

describe('rosbart-sticky-note', () => {
  it('renders', async () => {
    const page = await createPageWithPostIt('');
    const postIt = await page.find('rosbart-sticky-note');
    expect(postIt).toHaveClass('hydrated');
  });

  it('contains textarea', async () => {
    const page = await createPageWithPostIt('');
    const postIt = await findStickyNote(page);
    expect(postIt).toBeDefined();
  });

  it('is draggable', async () => {
    const page = await createPageWithPostIt("draggable=true");
    const postIt = await findStickyNote(page);
    expect(postIt.attributes.getNamedItem('draggable')).toBeTruthy();
  });

  it('is not draggable', async () => {
    const page = await createPageWithPostIt("draggable=false");
    const postIt = await findStickyNote(page);
    expect(postIt.attributes.getNamedItem('draggable')).toBeFalsy();
  });

  it('animates empty', async () => {
    const page = await createPageWithPostIt('');
    const postIt = await findStickyNote(page);
    expect(postIt).toHaveClass('wobble');
  });

  it('does not animate empty', async () => {
    const page = await createPageWithPostIt('animate-empty=false');
    const postIt = await findStickyNote(page);
    expect(postIt).not.toHaveClass('wobble');
  });

  it('renders as stack', async () => {
    const page = await createPageWithPostIt('stack=true');
    const postIt = await findStickyNote(page);
    expect(postIt).toHaveClass('paper-stack');
  });

  it('does not render as stack', async () => {
    const page = await createPageWithPostIt('stack=false');
    const postIt = await findStickyNote(page);
    expect(postIt).not.toHaveClass('paper-stack');
  });

  async function createPageWithPostIt(attributes: string): Promise<E2EPage> {
    const page = await newE2EPage();
    await page.setContent(`<rosbart-sticky-note ${attributes}></rosbart-sticky-note>`);
    return page;
  };

  async function findStickyNote(page: E2EPage): Promise<Element> {
    const element = await page.find('rosbart-sticky-note');
    return element.shadowRoot.querySelector('#sticky-note');
  };
});
