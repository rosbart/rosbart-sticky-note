import { Component, Prop, Element, Event, EventEmitter, Watch, h } from '@stencil/core';
import autosize from 'autosize';

@Component({
  tag: 'rosbart-sticky-note',
  styleUrl: 'rosbart-sticky-note.css',
  shadow: true
})
export class RosbartStickyNote {
  /**
   * The value to display
   */
  @Prop({ mutable: true }) value: string = '';

  /**
   * Flag to enable or disable native drag capabilities.
   */
  @Prop() draggable: boolean = false;

  /**
   * Enable or disable animated border for empty sticky-note.
   */
  @Prop() animateEmpty: boolean = true;

  /**
   * Set to true to render sticky-note stack instead of a single sticky-note.
   */
  @Prop() stack: boolean = false;

  /**
   * Set to true to make sticky-note readonly. Set to false to make sticky-note editable.
   */
  @Prop() readonly: boolean = false;

  @Element() private element: HTMLElement;

  /**
   * Text change event. Fired on each text change within sticky-note area.
   */
  @Event() textChange: EventEmitter;

  private postIt: HTMLElement;

  private postItTextarea: HTMLTextAreaElement;

  private valueChangeActive: boolean;

  componentDidLoad() {
    this.postIt = this.element.shadowRoot.querySelector('#sticky-note');
    this.postItTextarea = this.postIt.querySelector('#sticky-note-textarea');

    autosize(this.postItTextarea);

    this.updateAnimateEmptyProperty(this.animateEmpty);
    this.updateStackProperty(this.stack);
  }

  private onTextChange() {
    this.handleTextChange(() => {
      this.value = this.postItTextarea.value;
      this.textChange.emit(this.value);
      this.updateAnimateEmptyProperty(this.animateEmpty);
    });
  }

  private handleTextChange(callback: () => void) {
    if (!this.valueChangeActive) {
      this.valueChangeActive = true;
      callback();
      this.valueChangeActive = false;
    }
  }

  private updatePostItStyle(className: string, visible: boolean) {
    visible ? this.postIt.classList.add(className) : this.postIt.classList.remove(className);
  }

  @Watch('animateEmpty')
  updateAnimateEmptyProperty(newValue: boolean) {
    this.updatePostItStyle('wobble', newValue && (!this.value || this.value.trim() == ''));
  }

  @Watch('stack')
  updateStackProperty(newValue: boolean) {
    this.updatePostItStyle('paper-stack', newValue);
  }

  @Watch('value')
  updateValueProperty() {
    this.handleTextChange(() => {
      this.postItTextarea.value = this.value;
      this.postItTextarea.dispatchEvent(new CustomEvent('input'));
    });
  }

  render() {
    return <div id="sticky-note" class="sticky-note" draggable={this.draggable}>
      <div><slot name="top" /></div>

      <textarea id="sticky-note-textarea" class="sticky-note-textarea" readOnly={this.readonly} onInput={this.onTextChange.bind(this)}>{this.value}</textarea>

      <div><slot name="bottom" /></div>
    </div>
  }
}