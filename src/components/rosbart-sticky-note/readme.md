# rosbart-sticky-note



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description                                                                          | Type      | Default |
| -------------- | --------------- | ------------------------------------------------------------------------------------ | --------- | ------- |
| `animateEmpty` | `animate-empty` | Enable or disable animated border for empty sticky-note.                             | `boolean` | `true`  |
| `draggable`    | `draggable`     | Flag to enable or disable native drag capabilities.                                  | `boolean` | `false` |
| `readonly`     | `readonly`      | Set to true to make sticky-note readonly. Set to false to make sticky-note editable. | `boolean` | `false` |
| `stack`        | `stack`         | Set to true to render sticky-note stack instead of a single sticky-note.             | `boolean` | `false` |
| `value`        | `value`         | The value to display                                                                 | `string`  | `""`    |


## Events

| Event        | Description                                                           | Type               |
| ------------ | --------------------------------------------------------------------- | ------------------ |
| `textChange` | Text change event. Fired on each text change within sticky-note area. | `CustomEvent<any>` |


## CSS Custom Properties

| Name                          | Description                      |
| ----------------------------- | -------------------------------- |
| `--sticky-note-border-radius` | border-radius of the sticky-note |
| `--sticky-note-color`         | Background of the sticky-note    |
| `--sticky-note-display`       | display of the sticky-note       |
| `--sticky-note-font-family`   | font-family of the sticky-note   |
| `--sticky-note-font-size`     | font-size of the sticky-note     |
| `--sticky-note-height`        | height of the sticky-note        |
| `--sticky-note-margin`        | margin of the sticky-note        |
| `--sticky-note-max-height`    | max-height of the sticky-note    |
| `--sticky-note-max-width`     | max-width of the sticky-note     |
| `--sticky-note-min-height`    | min-height of the sticky-note    |
| `--sticky-note-min-width`     | min-width of the sticky-note     |
| `--sticky-note-overflow`      | overflow of the sticky-note      |
| `--sticky-note-padding`       | padding of the sticky-note       |
| `--sticky-note-text-align`    | text-align of the sticky-note    |
| `--sticky-note-white-space`   | white-space of the sticky-note   |
| `--sticky-note-width`         | width of the sticky-note         |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
