# rosbart-sticky-note

Simple sticky-note web component, built with ❤ and [stencil.js](https://stenciljs.com/).

- Default styling

<img src="./assets/default-sticky-note.png" height=200px>

- Changed color

<img src="./assets/colored-sticky-note.png" height=200px>

[Demo](https://rosbart.gitlab.io/rosbart-sticky-note/)

## Installation

```bash
    npm install https://gitlab.com/rosbart/rosbart-sticky-note.git#<version-of-latest-release>
```

## Component options

For further component options, such as parameters, css-variables and events, have a look at the auto-generated [component documentation](./src/components/rosbart-sticky-note/readme.md).

## Usage

```js
    import '@rosbart/rosbart-sticky-note'
```
```html
    <rosbart-sticky-note value="Text of the sticky-note"></rosbart-sticky-note>
```

For further information on how to integrate a stencil component in your framework have a look at the official [stencil documentation](https://stenciljs.com/docs/overview).

## Maintainer

* [Phibart](https://gitlab.com/phibart)
* [Josch Rossa](https://gitlab.com/josros)