import { Config } from '@stencil/core';

export const config: Config = {
  namespace: 'rosbart-sticky-note',
  outputTargets:[
    { type: 'dist' },
    { type: 'docs' },
    {
      type: 'www',
      baseUrl: process.env.NODE_ENV === 'production' ? '/rosbart-sticky-note/' : '/',
      serviceWorker: null // disable service workers
    }
  ]
};
